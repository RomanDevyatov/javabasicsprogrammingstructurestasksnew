public class NestedCycle {
    public void toPrintGrid(){
        int size = 5;
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col<=size;++col) {
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }


    public void toPrintGridMountain(){
        int size = 7;
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col<=size;++col) {
                if(row==size || col==size || col>=size-row+1)
                    System.out.print("# ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }


    public void toPrintGrid0(){
        int size = 7;
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col<=size;++col) {
                if(row<size && row>1 && col<size &&col>1)
                    System.out.print("  ");
                else
                    System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    public void toPrintGrid8(){
        int size = 7;
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col<=size;++col) {
                if (row == 1 || row == size) {
                    System.out.print("# ");
                    continue;
                }
                if(row==col || col==size-row+1) System.out.print("# ");
                else System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    public void toPrintGrid8Addition(){
        int size = 7;
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col<=size;++col) {
                if(row==size || row==1 || col==size ||col==1 || row==col || col==size-row+1)
                    System.out.print("# ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }
}
