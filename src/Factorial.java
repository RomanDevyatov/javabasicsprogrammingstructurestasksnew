public class Factorial {
    public int factorialCycle(int n){
        int res=1;
        for(int i = 1; i<=n;i++)
            res=res*i;

        return res;
    }

     public int factorialRecursion(int n){
        if(n==0)
            return 1;
         return n * factorialRecursion(n-1);
    }

    public void toCompareTime(int n){
        int tmp;

        long startTime = System.nanoTime();
        tmp = factorialRecursion(n);
        long factorialCycleTime = System.nanoTime() - startTime;

        startTime = System.nanoTime();
        tmp = factorialCycle(n);
        long factorialRecursion = System.nanoTime() - startTime;

        System.out.println("Cycle: "+ factorialCycleTime+"\n"+"Recursion: "+factorialRecursion);
    }
}
