import java.sql.SQLOutput;
import java.util.Arrays;

public class OneDimensionalArray {
    private int[] arr;

    public OneDimensionalArray(){
    }

    public void toCreateOddArray(){
        int n=50;
        this.arr = new int[n];
        for(int i = 0; i< n; i++){
            this.arr[i]=1+2*i;
        }
    }

    public int[] toCreateRandomArray(int n, int start, int finish){
        int[] tmp = new int[n];
        for(int i = 0; i< n; i++){
            tmp[i]=start+(int)(Math.random()*finish);
        }
        return tmp;
    }

    public void comparisonNum(int[] arr){
        int comp1=0;
        int comp0=0;
        int compMinus1=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]==1){
                comp1++;
            }else if(arr[i]==0){
                comp0++;
            }
            else if(arr[i]==-1){
                compMinus1++;
            }
        }

        if(comp1==comp0  && comp1==compMinus1){
            System.out.println("amount ==");
        } else if(comp1>comp0){
            if(compMinus1>comp1){
                System.out.println("amount of -1 more");
            }
            else System.out.println("amount of 1 more");
        }  else if(comp1<comp0){
            if(compMinus1>comp0){
                System.out.println("amount of -1 more");
            }
            else System.out.println("amount of 0 more");
        }
        else if(comp1==comp0){
            if(comp1>compMinus1){
                System.out.println("amount of 1 and 0 more");
            }
            else System.out.println("amount of -1 more");
        }
        else if(comp1==compMinus1){
            if(comp1>comp0){
                System.out.println("amount of 1 and -1 more");
            }
            else System.out.println("amount of 0 more");
        }
    }

    public int[] toCreateRandomArrayfor1(int n, int start, int finish){
        int[] tmp = new int[n];
        for(int i = 0; i< n; i++){
            tmp[i]=(int) Math.round(Math.random() * finish + start);
        }
        return tmp;
    }

    public int findIndexOfLast(int[] ar, int element){
        for(int i=ar.length-1;i>=0;i--){
            if(ar[i]==element){
                return i;
            }
        }
        return -1;
    }

    public int findMax(int[] arr){
        int max=arr[0];
        for(int i=0; i<arr.length;i++){
            if(arr[i]>max){
                max=arr[i];
            }
        }
        return max;
    }

    public int averageArray(int[] arr){
        int avg = 0;
        for(int i=0; i<arr.length; i++){
            avg=avg+arr[i];
        }
        return avg/arr.length;
    }

    public int findMin(int[] arr){
        int min=arr[0];
        for(int i=0; i<arr.length;i++){
            if(arr[i]<min){
                min=arr[i];
            }
        }
        return min;
    }

    public int toCalculateCountOddNum(int[] array){
        int oddNumCnt = 0;
        for(int i=0;i<array.length; i++){
            if(array[i]%2==1){
                oddNumCnt++;
            }
        }
        return oddNumCnt;
    }

    public int[] everyOddElement0(int[] arr){
        for (int i=1; i<arr.length; i=i+2){
            arr[i]=0;
        }
        return arr;
    }

    public void toPrint(int[] arr){
        for(int x: arr){
            System.out.print(x+" ");
        }
        System.out.println();
    }

    public void toPrint(){
        for(int x: arr){
            System.out.print(x+" ");
        }
        System.out.println();
    }

    public void toPrintDesc(){
        for(int i=this.arr.length-1; i>=0; --i){
            System.out.print(this.arr[i]+" ");
        }
        System.out.println();
    }
}
