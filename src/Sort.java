import java.util.Arrays;

public class Sort {
    public void sort(){
        int[] unsortedArr;
        int n= (int) (Math.random()*10);
        System.out.println("n: "+n);
        unsortedArr=new int[n];
        for(int i=0;i<n;i++) {
            unsortedArr[i] = (int) (Math.random() * 100);
        }
        int[] sortedArr;
        long startTime;


        System.out.println("Unsorted array: "+ Arrays.toString(unsortedArr));

        BubbleSort bs = new BubbleSort();
        startTime = System.nanoTime();
        sortedArr= bs.bubbleSort(unsortedArr);
        long bubbleEstimatedTime = System.nanoTime() - startTime;
        System.out.println("Bubble Sort: "+ Arrays.toString(sortedArr) + ", time: "+bubbleEstimatedTime);

        SelectionSort sb = new SelectionSort();
        startTime = System.nanoTime();
        sortedArr = sb.selectionSort(unsortedArr);
        long selectionEstimatedTime = System.nanoTime() - startTime;
        System.out.println("Selection Sort: " + Arrays.toString(sortedArr)+ ", time: "+selectionEstimatedTime);

        startTime = System.nanoTime();
        Arrays.sort(unsortedArr);
        long sortEstimatedTime = System.nanoTime() - startTime;
        System.out.println("Array Sort: " + Arrays.toString(unsortedArr)+ ", time: "+sortEstimatedTime);
    }
}
