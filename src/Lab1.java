import java.sql.SQLOutput;

class Lab1 {


    public static void main(String args[]){
        Sort arraySort=new Sort();
        arraySort.sort();

        Factorial factorial=new Factorial();
        int n=(int)(Math.random()*100);
        System.out.println("n : "+ n);
        factorial.toCompareTime(n);

        NestedCycle nestedcycle=new NestedCycle();
        nestedcycle.toPrintGrid();
        nestedcycle.toPrintGrid0();
        nestedcycle.toPrintGrid8();
        nestedcycle.toPrintGrid8Addition();
        nestedcycle.toPrintGridMountain();

        OneDimensionalArray oneDimensionalArray=new OneDimensionalArray();
        oneDimensionalArray.toCreateOddArray();
        oneDimensionalArray.toPrint();
        oneDimensionalArray.toPrintDesc();

        int[] randArray = oneDimensionalArray.toCreateRandomArray(20, 0,10);
        oneDimensionalArray.toPrint();
        final int oddCnt=oneDimensionalArray.toCalculateCountOddNum(randArray);
        System.out.println("Odd Num Count: " + oddCnt);
        final int EvenCnt=20-oddCnt;
        System.out.println("Even Num Count: " + EvenCnt);

        int[] randArray2 = oneDimensionalArray.toCreateRandomArray(10, 1,100);
        oneDimensionalArray.toPrint(randArray2);
        randArray2=oneDimensionalArray.everyOddElement0(randArray2);
        oneDimensionalArray.toPrint(randArray2);


        int[] randArray3 = oneDimensionalArray.toCreateRandomArray(15, -50,100);
        oneDimensionalArray.toPrint(randArray3);
        int max = oneDimensionalArray.findMax(randArray3);
        int min = oneDimensionalArray.findMin(randArray3);
        System.out.println("Max:"+max);
        System.out.println("MaxIndex:"+oneDimensionalArray.findIndexOfLast(randArray3, max));
        System.out.println("Min:"+min);
        System.out.println("MinIndex:"+oneDimensionalArray.findIndexOfLast(randArray3, min));
        System.out.println();

        int[] randArray4 = oneDimensionalArray.toCreateRandomArray(10, 0,10);
        int[] randArray5 = oneDimensionalArray.toCreateRandomArray(10, 0,10);
        oneDimensionalArray.toPrint(randArray4);
        oneDimensionalArray.toPrint(randArray5);
        if(oneDimensionalArray.averageArray(randArray4) > oneDimensionalArray.averageArray(randArray5)){
            System.out.println("Top avg > bottom avg");
        }
        else if(oneDimensionalArray.averageArray(randArray4) < oneDimensionalArray.averageArray(randArray5)){
            System.out.println("Top avg < bottom avg");
        }
        else System.out.println("Top avg == bottom avg");
        System.out.println();
        int[] randArray6 = oneDimensionalArray.toCreateRandomArrayfor1(20, -1,2);
        oneDimensionalArray.toPrint(randArray6);
        oneDimensionalArray.comparisonNum(randArray6);

        MultyDimensionalArray marr= new MultyDimensionalArray(8,8, 1,99);
        marr.show();
        System.out.println("Diag Sum: " + marr.sumDiag());
        System.out.println("Pob Diag Sum: " + marr.sumPobDiag());
        System.out.println();

        MultyDimensionalArray multyarr2 = new MultyDimensionalArray(8,5, -99, 198);
        multyarr2.show();
        System.out.println("Max:"+ multyarr2.findMax());
        System.out.println();
        System.out.println();
        MultyDimensionalArray multyarr3 = new MultyDimensionalArray(8,5, -10, 20);
        multyarr3.show();
        System.out.println("Index Max Mod Row:" + multyarr3.findIndexMaxModRow());
        System.out.println();
        MultyDimensionalArray multyarr4 = new MultyDimensionalArray(8,5, 0, 100);
        multyarr4.show();
        multyarr4.sortEachRowDesc();
        System.out.println();
        multyarr4.show();
    }
}

