public class MultyDimensionalArray {
    int[][] mas;
    int m;
    int n;

    public MultyDimensionalArray(int m, int n, int start, int cut){
        mas=new int[m][n];
        this.m=m;
        this.n=n;
        for(int i=0;i<m; i++){
            for(int j=0;j<n;j++){
                mas[i][j]=(int) Math.round(Math.random() * cut + start);
            }
        }
    }

    public int findMax(){
        int max=this.mas[0][0];
        for(int i=0; i<this.m;i++){
            for(int k=0;k<this.n; k++){
                if(mas[i][k]>max){
                    max=mas[i][k];
                }
            }

        }
        return max;
    }

    public int sumDiag(){
        int sum=0;
        if(this.m==this.n){
            for(int i=0;i<this.mas.length; i++){
                sum=sum+mas[i][i];
            }
        }
        return sum;
    }

    private void swap(int[] arr, int i1, int i2) {
        int tmp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = tmp;
    }

    public void sortEachRowDesc(){
        //electionSort ob=new SelectionSort();
        for(int i=0; i<this.m; i++){
            boolean needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int k = 1; k < this.n; k++) {
                    if (mas[i][k] > mas[i][k - 1]) {
                        swap(this.mas[i], k, k - 1);
                        needIteration = true;
                    }
                }
            }
        }
    }
    public int findIndexMaxModRow(){
        int rowMultMod=1;
        int indexMaxRow=-1;
        int max=0;
        for(int i=0; i<this.m;i++){
            for(int k=0;k<this.n; k++){
                rowMultMod=rowMultMod*Math.abs((this.mas[i][k]));
            }
            if(rowMultMod>max){
                max=rowMultMod;
                indexMaxRow=i;
            }
            rowMultMod=1;
        }
        return indexMaxRow;
    }

    public int sumPobDiag(){
        int sum=0;
        if(this.m==this.n){
            for(int i=this.mas.length-1; i>0; i--){
                sum=sum+mas[i][i];
            }
        }
        return sum;
    }

    public void show(){
        for(int i=0;i<this.m;i++){
            for(int j=0;j<this.n;j++){
                System.out.print(this.mas[i][j]+" ");
            }
            System.out.println();
        }
    }
}
