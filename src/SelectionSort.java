public class SelectionSort {

    int[] selectionSort(int arr[]) {
        int n = arr.length;
        int[] arrOut = new int[n];
        System.arraycopy (arr, 0, arrOut, 0, n);
        for (int i = 0; i < n - 1; i++) {
            int min_i = i;
            for (int j = i + 1; j < n; j++)
                if (arrOut[j] < arrOut[min_i])
                    min_i = j;

            int tmp = arrOut[min_i];
            arrOut[min_i] = arrOut[i];
            arrOut[i] = tmp;
        }
        return arrOut;
    }

}
