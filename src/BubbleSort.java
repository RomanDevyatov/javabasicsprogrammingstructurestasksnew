public class BubbleSort {

    int[] bubbleSort(int arr[])
    {
        int n = arr.length;
        int[] arrOut=new int[n];
        System.arraycopy (arr, 0, arrOut, 0, n);
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arrOut[j] > arrOut[j+1])
                {
                    // swap arr[j+1] and arr[i]
                    int tmp = arrOut[j];
                    arrOut[j] = arrOut[j+1];
                    arrOut[j+1] = tmp;
                }
        return arrOut;
    }
}
